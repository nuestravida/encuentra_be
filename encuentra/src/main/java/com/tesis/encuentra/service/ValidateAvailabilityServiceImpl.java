package com.tesis.encuentra.service;

import com.tesis.encuentra.model.command.ValidationAvailabilityRequest;
import com.tesis.encuentra.model.dto.UserDto;
import com.tesis.encuentra.model.dto.UserProfessionalDto;
import com.tesis.encuentra.repository.PersonProfessionalRepository;
import com.tesis.encuentra.service.interfaces.ValidateAvailabilityService;
import com.tesis.encuentra.util.Constant;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Time;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ValidateAvailabilityServiceImpl implements ValidateAvailabilityService {

    @Autowired
    PersonProfessionalRepository personProfessionalRepository;
    @Autowired
    ModelMapper modelMapper;

    @Override
    public List<UserProfessionalDto> validateAvailabilityProfessional(ValidationAvailabilityRequest validationAvailabilityRequest) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        LocalTime timeEndReservation = validationAvailabilityRequest.getTimeReservation().plusHours(validationAvailabilityRequest.getQuantityTimes());
        Time pruab = Time.valueOf(validationAvailabilityRequest.getTimeReservation());
        Time pruaba2 = Time.valueOf(timeEndReservation);
        return personProfessionalRepository.validateAvailabilityProfessional(validationAvailabilityRequest.getCategoryId()
                , validationAvailabilityRequest.getDateReservation(), pruab,
                Time.valueOf(timeEndReservation), Constant.STATUS_USER_ACTIVE).stream().map(user -> modelMapper.map(user, UserProfessionalDto.class)).collect(Collectors.toList());
    }
}
