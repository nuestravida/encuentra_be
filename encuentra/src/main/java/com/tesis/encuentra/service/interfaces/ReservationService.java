package com.tesis.encuentra.service.interfaces;

import com.tesis.encuentra.model.dto.ReservationDto;
import com.tesis.encuentra.model.entities.Reservation;

import java.util.List;

public interface ReservationService {

    List<ReservationDto> getByIdUser(int idUser);

    ReservationDto updateState(int id);

}
