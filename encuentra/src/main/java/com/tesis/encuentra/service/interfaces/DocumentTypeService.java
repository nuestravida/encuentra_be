package com.tesis.encuentra.service.interfaces;

import com.tesis.encuentra.model.dto.FixedDataDto;

import java.util.List;

public interface DocumentTypeService {
    List<FixedDataDto> getAll();
}
