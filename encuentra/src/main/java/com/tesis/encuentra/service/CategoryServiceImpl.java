package com.tesis.encuentra.service;

import com.tesis.encuentra.model.dto.FixedDataDto;
import com.tesis.encuentra.model.entities.Category;
import com.tesis.encuentra.repository.CategoryRepository;
import com.tesis.encuentra.service.interfaces.CategoryService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    CategoryRepository categoryRepository;

    @Autowired
    ModelMapper modelMapper;

    @Override
    public List<FixedDataDto> findByProfessionId(int idProfession) {
        return categoryRepository.findByProfessionId(idProfession).stream().map(category -> modelMapper.map(category, FixedDataDto.class)).collect(Collectors.toList());
    }
}
