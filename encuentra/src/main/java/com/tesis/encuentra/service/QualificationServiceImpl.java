package com.tesis.encuentra.service;

import com.tesis.encuentra.exception.BadRequestException;
import com.tesis.encuentra.model.command.QualificationRequest;
import com.tesis.encuentra.model.dto.QualificationDto;
import com.tesis.encuentra.model.entities.Qualification;
import com.tesis.encuentra.repository.QualificationRepository;
import com.tesis.encuentra.repository.ReservationRepository;
import com.tesis.encuentra.service.interfaces.QualificationService;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.modelmapper.spi.MatchingStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@Service
public class QualificationServiceImpl implements QualificationService {

    @Autowired
    public QualificationRepository qualificationRepository;

    @Autowired
    public ModelMapper modelMapper;

    @Autowired
    public ReservationRepository reservationRepository;

    @Autowired
    public MessageSource messageSource;

    @Override
    public void save(QualificationRequest qualificationRequest) {
        reservationRepository.findById(qualificationRequest.getReservationId()).orElseThrow(() -> new BadRequestException(messageSource.getMessage("validation.reservation.notExist", null, Locale.ROOT)));
        Qualification qualification = modelMapper.map(qualificationRequest, Qualification.class);
        qualificationRepository.save(qualification);
    }

    @Override
    public List<QualificationDto> getQualificationByReservation(int idUser) {
        return qualificationRepository.findByUserId(idUser).stream().map(qualification -> mapperCustomer(qualification)).collect(Collectors.toList());
    }

    private QualificationDto mapperCustomer(Qualification qualification) {
        QualificationDto qualificationDto = new QualificationDto();
        qualificationDto.setComment(qualification.getComment());
        qualificationDto.setScore(qualification.getScore());
        qualificationDto.setDateReservation(qualification.getReservation().getDateReservation().toString());
        qualificationDto.setFirstname(qualification.getReservation().getUser1().getFirstname());
        qualificationDto.setLastname(qualification.getReservation().getUser1().getLastname());
        return qualificationDto;
    }
}
