package com.tesis.encuentra.service.interfaces;

import com.tesis.encuentra.model.dto.EmailDto;

public interface MailManageService {

    int sendEmail(EmailDto email);

}
