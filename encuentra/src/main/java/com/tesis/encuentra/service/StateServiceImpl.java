package com.tesis.encuentra.service;

import com.tesis.encuentra.model.dto.FixedDataDto;
import com.tesis.encuentra.repository.StateRepository;
import com.tesis.encuentra.service.interfaces.StateService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class StateServiceImpl implements StateService {
    @Autowired
    StateRepository stateRepository;
    @Autowired
    ModelMapper modelMapper;

    public List<FixedDataDto> getByIdCountry(int idCountry) {
        return stateRepository.findByCountryId(idCountry).stream().map(city -> modelMapper.map(city, FixedDataDto.class)).collect(Collectors.toList());
    }
}
