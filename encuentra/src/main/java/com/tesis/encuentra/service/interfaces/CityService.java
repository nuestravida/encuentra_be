package com.tesis.encuentra.service.interfaces;

import com.mailjet.client.errors.MailjetException;
import com.mailjet.client.errors.MailjetSocketTimeoutException;
import com.tesis.encuentra.model.dto.FixedDataDto;

import java.io.IOException;
import java.util.List;

public interface CityService {
    List<FixedDataDto> getByIdState(int idState);
}
