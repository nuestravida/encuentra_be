package com.tesis.encuentra.service.interfaces;

public interface AWSService {

    String getSignedUrl(String pathObject);

}
