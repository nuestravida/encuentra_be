package com.tesis.encuentra.service;

import com.tesis.encuentra.model.dto.SocialNetworkDto;
import com.tesis.encuentra.repository.SocialNetworkRepository;
import com.tesis.encuentra.service.interfaces.SocialNetworkService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class SocialNetworkServiceImpl implements SocialNetworkService {

    @Autowired
    private SocialNetworkRepository socialNetworkRepository;
    @Autowired
    private ModelMapper modelMapper;

    @Override
    public List<SocialNetworkDto> getSocialNetworkByPersonProfessional(int idPersonProfessional) {
        return socialNetworkRepository.findByPersonProfessionalId(idPersonProfessional).stream().map(sn -> modelMapper.map(sn, SocialNetworkDto.class)).collect(Collectors.toList());
    }
}
