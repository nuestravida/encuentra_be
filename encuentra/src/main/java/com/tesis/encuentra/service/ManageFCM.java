package com.tesis.encuentra.service;

import com.tesis.encuentra.model.command.PushNotificationRequest;

public interface ManageFCM {

    String sendPnsToDevice(PushNotificationRequest pushNotificationRequest);

}
