package com.tesis.encuentra.service;

import com.sendgrid.Method;
import com.sendgrid.Request;
import com.sendgrid.Response;
import com.sendgrid.SendGrid;
import com.sendgrid.helpers.mail.Mail;
import com.sendgrid.helpers.mail.objects.*;
import com.tesis.encuentra.model.dto.EmailDto;
import com.tesis.encuentra.service.interfaces.MailManageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class MailManageServiceImpl implements MailManageService {

    private Logger logger = LoggerFactory.getLogger(MailManageServiceImpl.class);

    @Override
    public int sendEmail(EmailDto email) {
        Email from = new Email(email.getEmailFrom());
        Email to = new Email(email.getEmailTo());

        Personalization personalization = new Personalization();
        personalization.addTo(new Email(email.getEmailTo()));
        personalization.addDynamicTemplateData("password", email.getPassword());

        Content content = new Content("text/html", "ssss");
        Mail mail = new Mail(from, "dad", to, content);
        mail.setTemplateId("d-f5c5de21f7f343d4879071e42f04f5c4");
        mail.addPersonalization(personalization);
        SendGrid sg = new SendGrid("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");
        Request request = new Request();
        try {
            request.setMethod(Method.POST);
            request.setEndpoint("mail/send");
            request.setBody(mail.build());
            Response response = sg.api(request);
            return response.getStatusCode();
        } catch (IOException ex) {
            logger.error("Se presento un error al enviar el correo de recuperación de contraseña -> " + ex.getMessage());
            return 400;
        }
    }
}
