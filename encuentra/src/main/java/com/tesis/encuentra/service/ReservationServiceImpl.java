package com.tesis.encuentra.service;

import com.tesis.encuentra.exception.BadRequestException;
import com.tesis.encuentra.model.dto.ReservationDto;
import com.tesis.encuentra.model.entities.Qualification;
import com.tesis.encuentra.model.entities.Reservation;
import com.tesis.encuentra.model.entities.Status;
import com.tesis.encuentra.repository.ReservationRepository;
import com.tesis.encuentra.service.interfaces.ReservationService;
import com.tesis.encuentra.util.Constant;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ReservationServiceImpl implements ReservationService {

    @Autowired
    private ReservationRepository reservationRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public List<ReservationDto> getByIdUser(int idUser) {
        //List<Reservation> list = reservationRepository.findByUser(idUser);
        return reservationRepository.findByUser(idUser, Constant.STATUS_RESERVATION_RESERVED).stream().map(reservation -> modelMapper.map(reservation, ReservationDto.class)).collect(Collectors.toList());
    }

    @Override
    public ReservationDto updateState(int id) {
        Optional<Reservation> reservation = reservationRepository.findById(id);
        reservation.orElseThrow(() -> new BadRequestException("No se logro encontrar la reserva"));
        reservation.get().setStatus(new Status(Constant.STATUS_RESERVATION_FINISHED));
        return modelMapper.map(reservationRepository.save(reservation.get()), ReservationDto.class);
    }
}
