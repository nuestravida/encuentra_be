package com.tesis.encuentra.service.interfaces;

import com.tesis.encuentra.model.dto.SocialNetworkDto;

import java.util.List;

public interface SocialNetworkService {

    public List<SocialNetworkDto> getSocialNetworkByPersonProfessional(int idPersonProfessional);

}
