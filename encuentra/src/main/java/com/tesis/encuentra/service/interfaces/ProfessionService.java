package com.tesis.encuentra.service.interfaces;

import com.tesis.encuentra.model.dto.ProfessionDto;

import java.util.List;

public interface ProfessionService {
    List<ProfessionDto> getAll();
}
