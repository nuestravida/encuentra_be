package com.tesis.encuentra.service;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.HttpMethod;
import com.amazonaws.SdkClientException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import com.tesis.encuentra.config.AWSConfig;
import com.tesis.encuentra.service.interfaces.AWSService;
import com.tesis.encuentra.util.UtilField;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class AWSServiceImpl implements AWSService {

    @Autowired
    private AWSConfig awsConfig;

    @Value("${aws.s3.bucket}")
    private String bucketName;

    @Override
    public String getSignedUrl(String pathObject) {
        try {
            AmazonS3 s3Client = awsConfig.getAmazonS3Cient();
            GeneratePresignedUrlRequest g = new GeneratePresignedUrlRequest(bucketName, pathObject).withMethod(HttpMethod.GET).withExpiration(UtilField.expirationUrlAws());
            return s3Client.generatePresignedUrl(g).toString();
        } catch (AmazonServiceException e) {
            e.printStackTrace();
        } catch (SdkClientException e) {
            e.printStackTrace();
        }
        return null;
    }
}
