package com.tesis.encuentra.service;

import com.tesis.encuentra.model.dto.OptionDto;
import com.tesis.encuentra.repository.OptionRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class OptionService {

    @Autowired
    private OptionRepository optionRepository;

    @Autowired
    private ModelMapper modelMapper;

    public List<OptionDto> findByProfessionId(int idProfession) {
        return optionRepository.findByProfessionId(idProfession).stream().map(option -> modelMapper.map(option, OptionDto.class)).collect(Collectors.toList());
    }

}
