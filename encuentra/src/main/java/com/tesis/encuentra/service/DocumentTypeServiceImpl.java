package com.tesis.encuentra.service;

import com.tesis.encuentra.model.dto.FixedDataDto;
import com.tesis.encuentra.repository.DocumentTypeRepository;
import com.tesis.encuentra.service.interfaces.DocumentTypeService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class DocumentTypeServiceImpl implements DocumentTypeService {
    @Autowired
    DocumentTypeRepository documentTypeRepository;
    @Autowired
    ModelMapper modelMapper;

    @Override
    public List<FixedDataDto> getAll() {
        return documentTypeRepository.findAll().stream().map(documentType -> modelMapper.map(documentType, FixedDataDto.class)).collect(Collectors.toList());
    }
}
