package com.tesis.encuentra.service;

import com.tesis.encuentra.exception.BadRequestException;
import com.tesis.encuentra.model.dto.EmailDto;
import com.tesis.encuentra.model.command.RecoverPasswordRequest;
import com.tesis.encuentra.model.entities.User;
import com.tesis.encuentra.service.interfaces.RecoverPasswordService;
import com.tesis.encuentra.util.UtilField;
import com.tesis.encuentra.util.UtilSecurity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.time.temporal.ValueRange;
import java.util.Optional;
import java.util.Properties;

@Service
public class RecoverPasswordServiceImpl implements RecoverPasswordService {

    @Resource(name = "messages")
    private Properties properties;

    @Autowired
    private MailManageServiceImpl mailManageService;

    @Autowired
    private UserServiceImpl userService;

    @Override
    public void recoverPassword(RecoverPasswordRequest recoverPasswordDto) {
        Optional<User> userStored = userService.findById(recoverPasswordDto.getIdUser());
        if (!userStored.isPresent())
            throw new BadRequestException(properties.get("validation.user.noExist").toString());

        SecureRandom random = new SecureRandom();
        String password = new BigInteger(60, random).toString(8);

        EmailDto emailDto = new EmailDto();
        emailDto.setEmailFrom(properties.get("email.address.from").toString());
        emailDto.setPassword(password);
        emailDto.setEmailTo(recoverPasswordDto.getEmail());

        userStored.get().setPassword(UtilSecurity.generatePasswordRecover(password));
        userService.update(userStored.get());
        ValueRange valueRange = ValueRange.of(200, 299);
        if (!valueRange.isValidIntValue(mailManageService.sendEmail(emailDto))) {
            throw new BadRequestException("Error al recuperar contraseña");
        }
    }
}
