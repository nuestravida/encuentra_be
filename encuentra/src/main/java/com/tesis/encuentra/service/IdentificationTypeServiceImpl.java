package com.tesis.encuentra.service;

import com.tesis.encuentra.model.dto.FixedDataDto;
import com.tesis.encuentra.repository.IdentificationTypeRepository;
import com.tesis.encuentra.service.interfaces.IdentificationTypeService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class IdentificationTypeServiceImpl implements IdentificationTypeService {

    @Autowired
    IdentificationTypeRepository identificationTypeRepository;

    @Autowired
    ModelMapper modelMapper;

    @Override
    public List<FixedDataDto> getAll() {
        return identificationTypeRepository.findAll().stream().map(identificationType -> modelMapper.map(identificationType, FixedDataDto.class)).collect(Collectors.toList());
    }
}
