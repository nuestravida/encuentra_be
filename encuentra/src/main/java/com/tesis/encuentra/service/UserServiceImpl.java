package com.tesis.encuentra.service;

import com.tesis.encuentra.exception.BadRequestException;
import com.tesis.encuentra.model.command.UserProfessionalRequest;
import com.tesis.encuentra.model.command.UserRequest;
import com.tesis.encuentra.model.dto.UserDto;
import com.tesis.encuentra.model.entities.PersonProfessional;
import com.tesis.encuentra.model.entities.Role;
import com.tesis.encuentra.model.entities.Status;
import com.tesis.encuentra.model.entities.User;
import com.tesis.encuentra.repository.UserRepository;
import com.tesis.encuentra.service.interfaces.UserService;
import com.tesis.encuentra.util.Constant;
import com.tesis.encuentra.util.UtilField;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Locale;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private PersonProfessionalServiceImpl personProfessionalService;

    public UserDto save(UserRequest userRequest) {

        if (userRepository.findByEmail(userRequest.getEmail()).isPresent()) {
            throw new BadRequestException(messageSource.getMessage("user.validation.email_no_exist", null, Locale.ROOT));
        }

        BCryptPasswordEncoder brBCryptPasswordEncoder = new BCryptPasswordEncoder();
        userRequest.setPassword(brBCryptPasswordEncoder.encode(userRequest.getPassword()));
        User user = modelMapper.map(userRequest, User.class);
        user.setUsername(userRequest.getEmail());
        user.setStatus(new Status(Constant.STATUS_USER_ACTIVE));
        user.setRole(new Role(Constant.ROLE_USER));

        return modelMapper.map(userRepository.save(user), UserDto.class);
    }

    public UserDto saveUserProfessional(UserProfessionalRequest userProfessionalRequest) {

        if (userRepository.findByEmail(userProfessionalRequest.getEmail()).isPresent()) {
            throw new BadRequestException(messageSource.getMessage("user.validation.email_no_exist", null, Locale.ROOT));
        }

        BCryptPasswordEncoder brBCryptPasswordEncoder = new BCryptPasswordEncoder();
        userProfessionalRequest.setPassword(brBCryptPasswordEncoder.encode(userProfessionalRequest.getPassword()));
        User user = modelMapper.map(userProfessionalRequest, User.class);
        user.setUsername(userProfessionalRequest.getEmail());
        user.setStatus(new Status(Constant.STATUS_USER_ACTIVE));
        user.setRole(new Role(Constant.ROLE_PROFESSIONAL));

        return modelMapper.map(userRepository.save(user), UserDto.class);
    }

    @Override
    public UserDto update(User user) {
        return modelMapper.map(userRepository.save(user), UserDto.class);
    }

    @Override
    public Optional<User> findById(int id) {
        Optional<User> r = userRepository.findById(id);
        return r;
    }

    @Override
    public Optional<UserDto> findByUsername(String username) {
        Optional<User> user = userRepository.findByUsername(username);
        if (user.isPresent()) {
            return Optional.of(modelMapper.map(user.get(), UserDto.class));
        }
        return Optional.empty();
    }

    private PersonProfessional createPersonProfessional(UserRequest userRequest) {
        return personProfessionalService.save(userRequest);
    }


}
