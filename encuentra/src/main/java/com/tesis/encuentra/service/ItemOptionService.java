package com.tesis.encuentra.service;

import com.tesis.encuentra.model.dto.ItemOptionDto;
import com.tesis.encuentra.model.entities.ItemOption;
import com.tesis.encuentra.repository.ItemOptionRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ItemOptionService {

    @Autowired
    private ItemOptionRepository itemOptionRepository;

    @Autowired
    private ModelMapper modelMapper;

    public List<ItemOptionDto> findByProfession(int idProfession) {
        return itemOptionRepository.findByProfession(idProfession).stream().map(itemOption -> modelMapper.map(itemOption, ItemOptionDto.class)).collect(Collectors.toList());
    }

}
