package com.tesis.encuentra.service;

import com.tesis.encuentra.model.dto.FixedDataDto;
import com.tesis.encuentra.repository.GenderRepository;
import com.tesis.encuentra.service.interfaces.GenderService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class GenderServiceImpl implements GenderService {
    @Autowired
    GenderRepository genderRepository;
    @Autowired
    ModelMapper modelMapper;

    @Override
    public List<FixedDataDto> getAll() {
        return genderRepository.findAll().stream().map(gender -> modelMapper.map(gender, FixedDataDto.class)).collect(Collectors.toList());
    }
}
