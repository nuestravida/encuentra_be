package com.tesis.encuentra.service.interfaces;

import com.tesis.encuentra.model.command.QualificationRequest;
import com.tesis.encuentra.model.dto.QualificationDto;

import java.util.List;

public interface QualificationService {

    void save(QualificationRequest qualificationRequest);

    List<QualificationDto>  getQualificationByReservation(int idUser);
}
