package com.tesis.encuentra.service;

import com.amazonaws.HttpMethod;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import com.tesis.encuentra.config.AWSConfig;
import com.tesis.encuentra.model.dto.FileDto;
import com.tesis.encuentra.model.dto.ProfessionDto;
import com.tesis.encuentra.model.entities.File;
import com.tesis.encuentra.model.entities.Profession;
import com.tesis.encuentra.repository.ProfessionRepository;
import com.tesis.encuentra.service.interfaces.ProfessionService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProfessionServiceImpl implements ProfessionService {

    @Autowired
    ProfessionRepository professionRepository;
    @Autowired
    ModelMapper modelMapper;
    @Autowired
    private AWSServiceImpl awsService;

    public List<ProfessionDto> getAll() {
        List<Profession> listProfessions = professionRepository.findFileAll();
        listProfessions.stream().forEach(profession -> {
            profession.setFile(new File(awsService.getSignedUrl(profession.getFile().getPath())));
        });
        return listProfessions.stream().map(profession -> modelMapper.map(profession, ProfessionDto.class)).collect(Collectors.toList());
    }
}
