package com.tesis.encuentra.service;

import com.tesis.encuentra.model.command.UserRequest;
import com.tesis.encuentra.model.entities.PersonProfessional;
import com.tesis.encuentra.repository.PersonProfessionalRepository;
import com.tesis.encuentra.service.interfaces.PersonProfessionalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PersonProfessionalServiceImpl implements PersonProfessionalService {

    @Autowired
    private PersonProfessionalRepository personProfessionalRepository;

    @Override
    public PersonProfessional save(UserRequest userRequest) {
        PersonProfessional personProfessional = new PersonProfessional();
        //personProfessional.setIdentificationType(userRequest.getIdentificationType());
        //personProfessional.setIdentificationNumber(userRequest.getIdentificationNumber());
        return personProfessionalRepository.save(personProfessional);
    }
}
