package com.tesis.encuentra.service.interfaces;

import com.tesis.encuentra.model.dto.ItemDto;

import java.util.List;

public interface ItemService {
    List<ItemDto> getSubItem(int idCategory);
}
