package com.tesis.encuentra.service.interfaces;

import com.tesis.encuentra.model.command.UserProfessionalRequest;
import com.tesis.encuentra.model.command.UserRequest;
import com.tesis.encuentra.model.dto.UserDto;
import com.tesis.encuentra.model.entities.User;

import java.util.Optional;

public interface UserService {
    UserDto save(UserRequest userRequest);

    UserDto saveUserProfessional(UserProfessionalRequest userProfessionalRequest);

    UserDto update(User user);

    Optional<User> findById(int id);

    Optional<UserDto> findByUsername(String username);
}
