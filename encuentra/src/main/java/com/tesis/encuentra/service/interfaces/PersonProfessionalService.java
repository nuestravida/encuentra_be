package com.tesis.encuentra.service.interfaces;

import com.tesis.encuentra.model.command.UserRequest;
import com.tesis.encuentra.model.entities.PersonProfessional;

public interface PersonProfessionalService {
    PersonProfessional save(UserRequest userRequest);
}
