package com.tesis.encuentra.service;
import com.tesis.encuentra.model.dto.FixedDataDto;
import com.tesis.encuentra.repository.CityRepository;
import com.tesis.encuentra.service.interfaces.CityService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CityServiceImpl implements CityService {

    @Autowired
    CityRepository cityRepository;
    @Autowired
    ModelMapper modelMapper;

    @Override
    public List<FixedDataDto> getByIdState(int idState) {
        return cityRepository.findByStateId(idState).stream().map(city -> modelMapper.map(city, FixedDataDto.class)).collect(Collectors.toList());
    }
}
