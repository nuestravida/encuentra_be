package com.tesis.encuentra.service.interfaces;

import com.tesis.encuentra.model.command.RecoverPasswordRequest;
import org.apache.http.HttpResponse;

public interface RecoverPasswordService {

    void recoverPassword(RecoverPasswordRequest recoverPasswordDto);

}
