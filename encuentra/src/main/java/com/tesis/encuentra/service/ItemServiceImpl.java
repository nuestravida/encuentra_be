package com.tesis.encuentra.service;

import com.tesis.encuentra.model.dto.ItemDto;
import com.tesis.encuentra.model.dto.SubItemDto;
import com.tesis.encuentra.model.entities.Item;
import com.tesis.encuentra.repository.SubItemRepository;
import com.tesis.encuentra.service.interfaces.ItemService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ItemServiceImpl implements ItemService {

    @Autowired
    private SubItemRepository subItemRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public List<ItemDto> getSubItem(int idCategory) {
        List<ItemDto> listItemDto = new ArrayList<>();
        for (Item item : subItemRepository.findBySubItemByCategoryId(idCategory)) {
            ItemDto itemDto = modelMapper.map(item, ItemDto.class);
            itemDto.subItemDto = item.getSubitems().stream().map(subItem -> modelMapper.map(subItem, SubItemDto.class)).collect(Collectors.toList());
            listItemDto.add(itemDto);
        }
        return listItemDto;
    }
}
