package com.tesis.encuentra.service.interfaces;

import com.tesis.encuentra.model.command.ValidationAvailabilityRequest;
import com.tesis.encuentra.model.dto.UserDto;
import com.tesis.encuentra.model.dto.UserProfessionalDto;

import java.util.List;

public interface ValidateAvailabilityService {

    List<UserProfessionalDto> validateAvailabilityProfessional(ValidationAvailabilityRequest validationAvailabilityRequest);
}
