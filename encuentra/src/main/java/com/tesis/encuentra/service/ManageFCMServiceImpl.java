package com.tesis.encuentra.service;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.google.firebase.messaging.Message;
import com.google.firebase.messaging.Notification;
import com.tesis.encuentra.model.command.PushNotificationRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;

import javax.annotation.PostConstruct;
import java.io.IOException;

public class ManageFCMServiceImpl implements ManageFCM {

    @Value("${app.firebase-configuration-file}")
    private String firebaseConfigPath;

    Logger logger = LoggerFactory.getLogger(ManageFCMServiceImpl.class);

    @PostConstruct
    public void initialize() {
        try {
            FirebaseOptions options = new FirebaseOptions.Builder()
                    .setCredentials(GoogleCredentials.fromStream(new ClassPathResource(firebaseConfigPath).getInputStream())).build();
            if (FirebaseApp.getApps().isEmpty()) {
                FirebaseApp.initializeApp(options);
                logger.info("La aplicación de firebase ha sido inicializada correctamente");
            }
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
    }


    @Override
    public String sendPnsToDevice(PushNotificationRequest pushNotificationRequest) {
        Message message = Message.builder()
                .setToken(pushNotificationRequest.getToken())
                .setNotification(new Notification(pushNotificationRequest.getTitle(), pushNotificationRequest.getMessage()))
                .putData("content", pushNotificationRequest.getTitle())
                .putData("body", pushNotificationRequest.getMessage())
                .build();

        String response = null;
        try {
            response = FirebaseMessaging.getInstance().send(message);
        } catch (FirebaseMessagingException e) {
            System.out.println("error al enviar np -> " + e.getMessage());
        }
        return response;
    }
}
