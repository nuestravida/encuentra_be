package com.tesis.encuentra.model.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ItemDto {
    public int id;
    public String title;
    public String description;
    public List<SubItemDto> subItemDto;
}
