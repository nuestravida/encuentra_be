package com.tesis.encuentra.model.entities;

import javax.persistence.*;

/**
 * The persistent class for the social_networks database table.
 */
@Entity
@Table(name = "social_networks")
@NamedQuery(name = "SocialNetwork.findAll", query = "SELECT s FROM SocialNetwork s")
public class SocialNetwork {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private String path;

    private String code;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "person_professional_id")
    private PersonProfessional personProfessional;

    public SocialNetwork() {
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public PersonProfessional getPersonProfessional() {
        return personProfessional;
    }

    public void setPersonProfessional(PersonProfessional personProfessional) {
        this.personProfessional = personProfessional;
    }
}
