package com.tesis.encuentra.model.command;

import com.tesis.encuentra.model.entities.Gender;

import javax.validation.constraints.NotBlank;

public class UserProfessionalRequest {

    @NotBlank(message = "{validation.email.notEmpty}")
    private String email;
    @NotBlank(message = "{validation.firstName.notEmpty}")
    private String firstName;
    @NotBlank(message = "{validation.lastName.notEmpty}")
    private String lastName;
    @NotBlank(message = "{validation.password.notEmpty}")
    private String password;
    private String username;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
