package com.tesis.encuentra.model.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the subitems database table.
 * 
 */
@Entity
@Table(name="subitems")
@NamedQuery(name="Subitem.findAll", query="SELECT s FROM Subitem s")
public class Subitem implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;

	@Column(name="minimum_time")
	private int minimumTime;

	private int price;

	private String title;

	//bi-directional many-to-one association to Item
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="items_id")
	private Item item;

	//bi-directional many-to-many association to Reservation
	@ManyToMany
	@JoinTable(
		name="subitem_reservation"
		, joinColumns={
			@JoinColumn(name="subitem_id")
			}
		, inverseJoinColumns={
			@JoinColumn(name="reservation_id")
			}
		)
	private List<Reservation> reservations;

	public Subitem() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getMinimumTime() {
		return this.minimumTime;
	}

	public void setMinimumTime(int minimumTime) {
		this.minimumTime = minimumTime;
	}

	public int getPrice() {
		return this.price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Item getItem() {
		return this.item;
	}

	public void setItem(Item item) {
		this.item = item;
	}

	public List<Reservation> getReservations() {
		return this.reservations;
	}

	public void setReservations(List<Reservation> reservations) {
		this.reservations = reservations;
	}

}