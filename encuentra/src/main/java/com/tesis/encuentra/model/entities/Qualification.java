package com.tesis.encuentra.model.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the qualifications database table.
 *
 */
@Entity
@Table(name="qualifications")
@NamedQuery(name="Qualification.findAll", query="SELECT c FROM Qualification c")
public class Qualification implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private int id;

    private String comment;

    private BigDecimal score;

    //bi-directional many-to-one association to Reservation
    @ManyToOne(fetch=FetchType.LAZY)
    private Reservation reservation;

    public Qualification() {
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getComment() {
        return this.comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public BigDecimal getScore() {
        return this.score;
    }

    public void setScore(BigDecimal score) {
        this.score = score;
    }

    public Reservation getReservation() {
        return this.reservation;
    }

    public void setReservation(Reservation reservation) {
        this.reservation = reservation;
    }

}