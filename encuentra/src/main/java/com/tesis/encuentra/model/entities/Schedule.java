package com.tesis.encuentra.model.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Time;
import java.util.Date;


/**
 * The persistent class for the schedules database table.
 * 
 */
@Entity
@Table(name="schedules")
@NamedQuery(name="Schedule.findAll", query="SELECT s FROM Schedule s")
public class Schedule implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;

	@Column(name="all_day")
	private byte allDay;

	@Temporal(TemporalType.DATE)
	private Date date;

	@Column(name="time_end")
	private Time timeEnd;

	@Column(name="time_start")
	private Time timeStart;

	//bi-directional many-to-one association to PersonProfessional
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="person_professional_id")
	private PersonProfessional personProfessional;

	public Schedule() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public byte getAllDay() {
		return this.allDay;
	}

	public void setAllDay(byte allDay) {
		this.allDay = allDay;
	}

	public Date getDate() {
		return this.date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Time getTimeEnd() {
		return this.timeEnd;
	}

	public void setTimeEnd(Time timeEnd) {
		this.timeEnd = timeEnd;
	}

	public Time getTimeStart() {
		return this.timeStart;
	}

	public void setTimeStart(Time timeStart) {
		this.timeStart = timeStart;
	}

	public PersonProfessional getPersonProfessional() {
		return this.personProfessional;
	}

	public void setPersonProfessional(PersonProfessional personProfessional) {
		this.personProfessional = personProfessional;
	}

}