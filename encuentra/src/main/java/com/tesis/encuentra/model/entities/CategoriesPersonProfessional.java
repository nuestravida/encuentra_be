package com.tesis.encuentra.model.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigInteger;


/**
 * The persistent class for the categories_person_professionals database table.
 * 
 */
@Entity
@Table(name="categories_person_professionals")
@NamedQuery(name="CategoriesPersonProfessional.findAll", query="SELECT c FROM CategoriesPersonProfessional c")
public class CategoriesPersonProfessional implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;

	private String description;

	private BigInteger rate;

	//bi-directional many-to-one association to Category
	@ManyToOne(fetch=FetchType.LAZY)
	private Category category;

	//bi-directional many-to-one association to PersonProfessional
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="person_professional_id")
	private PersonProfessional personProfessional;

	public CategoriesPersonProfessional() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public BigInteger getRate() {
		return this.rate;
	}

	public void setRate(BigInteger rate) {
		this.rate = rate;
	}

	public Category getCategory() {
		return this.category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public PersonProfessional getPersonProfessional() {
		return this.personProfessional;
	}

	public void setPersonProfessional(PersonProfessional personProfessional) {
		this.personProfessional = personProfessional;
	}

}