package com.tesis.encuentra.model.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the categories database table.
 * 
 */
@Entity
@Table(name="categories")
@NamedQuery(name="Category.findAll", query="SELECT c FROM Category c")
public class Category implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;

	private String description;

	private String name;

	//bi-directional many-to-one association to Profession
	@ManyToOne(fetch=FetchType.LAZY)
	private Profession profession;

	//bi-directional many-to-one association to CategoriesPersonProfessional
	@OneToMany(mappedBy="category")
	private List<CategoriesPersonProfessional> categoriesPersonProfessionals;

	//bi-directional many-to-many association to Item
	@ManyToMany(mappedBy="categories")
	private List<Item> items;

	//bi-directional many-to-one association to ItemsCategory
	@OneToMany(mappedBy="category")
	private List<ItemsCategory> itemsCategories;

	public Category() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Profession getProfession() {
		return this.profession;
	}

	public void setProfession(Profession profession) {
		this.profession = profession;
	}

	public List<CategoriesPersonProfessional> getCategoriesPersonProfessionals() {
		return this.categoriesPersonProfessionals;
	}

	public void setCategoriesPersonProfessionals(List<CategoriesPersonProfessional> categoriesPersonProfessionals) {
		this.categoriesPersonProfessionals = categoriesPersonProfessionals;
	}

	public CategoriesPersonProfessional addCategoriesPersonProfessional(CategoriesPersonProfessional categoriesPersonProfessional) {
		getCategoriesPersonProfessionals().add(categoriesPersonProfessional);
		categoriesPersonProfessional.setCategory(this);

		return categoriesPersonProfessional;
	}

	public CategoriesPersonProfessional removeCategoriesPersonProfessional(CategoriesPersonProfessional categoriesPersonProfessional) {
		getCategoriesPersonProfessionals().remove(categoriesPersonProfessional);
		categoriesPersonProfessional.setCategory(null);

		return categoriesPersonProfessional;
	}

	public List<Item> getItems() {
		return this.items;
	}

	public void setItems(List<Item> items) {
		this.items = items;
	}

	public List<ItemsCategory> getItemsCategories() {
		return this.itemsCategories;
	}

	public void setItemsCategories(List<ItemsCategory> itemsCategories) {
		this.itemsCategories = itemsCategories;
	}

	public ItemsCategory addItemsCategory(ItemsCategory itemsCategory) {
		getItemsCategories().add(itemsCategory);
		itemsCategory.setCategory(this);

		return itemsCategory;
	}

	public ItemsCategory removeItemsCategory(ItemsCategory itemsCategory) {
		getItemsCategories().remove(itemsCategory);
		itemsCategory.setCategory(null);

		return itemsCategory;
	}

}