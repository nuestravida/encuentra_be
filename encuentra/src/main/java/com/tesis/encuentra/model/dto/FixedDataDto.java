package com.tesis.encuentra.model.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FixedDataDto {
    private int id;
    private String name;
}
