package com.tesis.encuentra.model.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the person_professionals database table.
 * 
 */
@Entity
@Table(name="person_professionals")
@NamedQuery(name="PersonProfessional.findAll", query="SELECT p FROM PersonProfessional p")
public class PersonProfessional implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;

	@Column(name="identification_number")
	private String identificationNumber;

	//bi-directional many-to-one association to CategoriesPersonProfessional
	@OneToMany(mappedBy="personProfessional")
	private List<CategoriesPersonProfessional> categoriesPersonProfessionals;

	//bi-directional many-to-one association to File
	@ManyToOne(fetch=FetchType.LAZY)
	private File file;

	//bi-directional many-to-one association to IdentificationType
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="identification_type_id")
	private IdentificationType identificationType;

	//bi-directional many-to-one association to PersonProfessionalsCity
	@OneToMany(mappedBy="personProfessional")
	private List<PersonProfessionalsCity> personProfessionalsCities;

	//bi-directional many-to-one association to Schedule
	@OneToMany(mappedBy="personProfessional")
	private List<Schedule> schedules;

	//bi-directional many-to-one association to User
	@OneToMany(mappedBy="personProfessional")
	private List<User> users;

	public PersonProfessional() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getIdentificationNumber() {
		return this.identificationNumber;
	}

	public void setIdentificationNumber(String identificationNumber) {
		this.identificationNumber = identificationNumber;
	}

	public List<CategoriesPersonProfessional> getCategoriesPersonProfessionals() {
		return this.categoriesPersonProfessionals;
	}

	public void setCategoriesPersonProfessionals(List<CategoriesPersonProfessional> categoriesPersonProfessionals) {
		this.categoriesPersonProfessionals = categoriesPersonProfessionals;
	}

	public CategoriesPersonProfessional addCategoriesPersonProfessional(CategoriesPersonProfessional categoriesPersonProfessional) {
		getCategoriesPersonProfessionals().add(categoriesPersonProfessional);
		categoriesPersonProfessional.setPersonProfessional(this);

		return categoriesPersonProfessional;
	}

	public CategoriesPersonProfessional removeCategoriesPersonProfessional(CategoriesPersonProfessional categoriesPersonProfessional) {
		getCategoriesPersonProfessionals().remove(categoriesPersonProfessional);
		categoriesPersonProfessional.setPersonProfessional(null);

		return categoriesPersonProfessional;
	}

	public File getFile() {
		return this.file;
	}

	public void setFile(File file) {
		this.file = file;
	}

	public IdentificationType getIdentificationType() {
		return this.identificationType;
	}

	public void setIdentificationType(IdentificationType identificationType) {
		this.identificationType = identificationType;
	}

	public List<PersonProfessionalsCity> getPersonProfessionalsCities() {
		return this.personProfessionalsCities;
	}

	public void setPersonProfessionalsCities(List<PersonProfessionalsCity> personProfessionalsCities) {
		this.personProfessionalsCities = personProfessionalsCities;
	}

	public PersonProfessionalsCity addPersonProfessionalsCity(PersonProfessionalsCity personProfessionalsCity) {
		getPersonProfessionalsCities().add(personProfessionalsCity);
		personProfessionalsCity.setPersonProfessional(this);

		return personProfessionalsCity;
	}

	public PersonProfessionalsCity removePersonProfessionalsCity(PersonProfessionalsCity personProfessionalsCity) {
		getPersonProfessionalsCities().remove(personProfessionalsCity);
		personProfessionalsCity.setPersonProfessional(null);

		return personProfessionalsCity;
	}

	public List<Schedule> getSchedules() {
		return this.schedules;
	}

	public void setSchedules(List<Schedule> schedules) {
		this.schedules = schedules;
	}

	public Schedule addSchedule(Schedule schedule) {
		getSchedules().add(schedule);
		schedule.setPersonProfessional(this);

		return schedule;
	}

	public Schedule removeSchedule(Schedule schedule) {
		getSchedules().remove(schedule);
		schedule.setPersonProfessional(null);

		return schedule;
	}

	public List<User> getUsers() {
		return this.users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public User addUser(User user) {
		getUsers().add(user);
		user.setPersonProfessional(this);

		return user;
	}

	public User removeUser(User user) {
		getUsers().remove(user);
		user.setPersonProfessional(null);

		return user;
	}

}