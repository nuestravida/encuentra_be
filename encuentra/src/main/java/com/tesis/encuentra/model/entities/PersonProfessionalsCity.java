package com.tesis.encuentra.model.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the person_professionals_cities database table.
 * 
 */
@Entity
@Table(name="person_professionals_cities")
@NamedQuery(name="PersonProfessionalsCity.findAll", query="SELECT p FROM PersonProfessionalsCity p")
public class PersonProfessionalsCity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;

	//bi-directional many-to-one association to City
	@ManyToOne(fetch=FetchType.LAZY)
	private City city;

	//bi-directional many-to-one association to PersonProfessional
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="person_professional_id")
	private PersonProfessional personProfessional;

	public PersonProfessionalsCity() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public City getCity() {
		return this.city;
	}

	public void setCity(City city) {
		this.city = city;
	}

	public PersonProfessional getPersonProfessional() {
		return this.personProfessional;
	}

	public void setPersonProfessional(PersonProfessional personProfessional) {
		this.personProfessional = personProfessional;
	}

}