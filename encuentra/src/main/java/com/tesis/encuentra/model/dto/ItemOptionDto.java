package com.tesis.encuentra.model.dto;

import com.tesis.encuentra.model.entities.Option;

import java.util.List;

public class ItemOptionDto {

    private int id;

    private String name;

    private List<OptionDto> options;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<OptionDto> getOptions() {
        return options;
    }

    public void setOptions(List<OptionDto> options) {
        this.options = options;
    }
}
