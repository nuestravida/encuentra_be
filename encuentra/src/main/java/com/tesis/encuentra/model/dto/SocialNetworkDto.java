package com.tesis.encuentra.model.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class SocialNetworkDto {

    private String path;
    private String code;

}
