package com.tesis.encuentra.model.command;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

public class RecoverPasswordRequest {

    @Min(1)
    private int idUser;
    @NotBlank
    private String email;

    public int getIdUser() {
        return idUser;
    }


    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
