package com.tesis.encuentra.model.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the professions database table.
 * 
 */
@Entity
@Table(name="professions")
@NamedQuery(name="Profession.findAll", query="SELECT p FROM Profession p")
public class Profession implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;

	private String description;

	private String name;

	//bi-directional many-to-one association to Category
	@OneToMany(mappedBy="profession")
	private List<Category> categories;

	//bi-directional many-to-one association to File
	@ManyToOne(fetch=FetchType.LAZY)
	private File file;

	public Profession() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Category> getCategories() {
		return this.categories;
	}

	public void setCategories(List<Category> categories) {
		this.categories = categories;
	}

	public Category addCategory(Category category) {
		getCategories().add(category);
		category.setProfession(this);

		return category;
	}

	public Category removeCategory(Category category) {
		getCategories().remove(category);
		category.setProfession(null);

		return category;
	}

	public File getFile() {
		return this.file;
	}

	public void setFile(File file) {
		this.file = file;
	}

}