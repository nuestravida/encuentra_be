package com.tesis.encuentra.model.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the items database table.
 * 
 */
@Entity
@Table(name="items")
@NamedQuery(name="Item.findAll", query="SELECT i FROM Item i")
public class Item implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;

	private String description;

	private String title;

	//bi-directional many-to-many association to Category
	@ManyToMany
	@JoinTable(
		name="items_categories"
		, joinColumns={
			@JoinColumn(name="item_id")
			}
		, inverseJoinColumns={
			@JoinColumn(name="category_id")
			}
		)
	private List<Category> categories;

	//bi-directional many-to-one association to ItemsCategory
	@OneToMany(mappedBy="item")
	private List<ItemsCategory> itemsCategories;

	//bi-directional many-to-one association to Subitem
	@OneToMany(mappedBy="item")
	private List<Subitem> subitems;

	public Item() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<Category> getCategories() {
		return this.categories;
	}

	public void setCategories(List<Category> categories) {
		this.categories = categories;
	}

	public List<ItemsCategory> getItemsCategories() {
		return this.itemsCategories;
	}

	public void setItemsCategories(List<ItemsCategory> itemsCategories) {
		this.itemsCategories = itemsCategories;
	}

	public ItemsCategory addItemsCategory(ItemsCategory itemsCategory) {
		getItemsCategories().add(itemsCategory);
		itemsCategory.setItem(this);

		return itemsCategory;
	}

	public ItemsCategory removeItemsCategory(ItemsCategory itemsCategory) {
		getItemsCategories().remove(itemsCategory);
		itemsCategory.setItem(null);

		return itemsCategory;
	}

	public List<Subitem> getSubitems() {
		return this.subitems;
	}

	public void setSubitems(List<Subitem> subitems) {
		this.subitems = subitems;
	}

	public Subitem addSubitem(Subitem subitem) {
		getSubitems().add(subitem);
		subitem.setItem(this);

		return subitem;
	}

	public Subitem removeSubitem(Subitem subitem) {
		getSubitems().remove(subitem);
		subitem.setItem(null);

		return subitem;
	}

}