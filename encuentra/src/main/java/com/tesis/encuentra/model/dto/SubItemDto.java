package com.tesis.encuentra.model.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SubItemDto {
    public int id;
    public String title;
    public int price;
    public int minimumTime;
}
