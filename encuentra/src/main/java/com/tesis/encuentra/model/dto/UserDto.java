package com.tesis.encuentra.model.dto;

import com.tesis.encuentra.model.entities.Gender;
import com.tesis.encuentra.model.entities.Role;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserDto {
    public String id;
    public String email;
    public String firstName;
    public String lastName;
    public String password;
    public String token;
    public String phone;
    public FixedDataDto gender;
    public FixedDataDto role;
}
