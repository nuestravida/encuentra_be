package com.tesis.encuentra.model.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the users database table.
 * 
 */
@Entity
@Table(name="users")
@NamedQuery(name="User.findAll", query="SELECT u FROM User u")
public class User implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;

	private String email;

	private String firstname;

	private String lastname;

	private String password;

	private String phone;

	private String username;

	//bi-directional many-to-one association to Reservation
	@OneToMany(mappedBy="user1")
	private List<Reservation> reservations1;

	//bi-directional many-to-one association to Reservation
	@OneToMany(mappedBy="user2")
	private List<Reservation> reservations2;

	//bi-directional many-to-one association to Gender
	@ManyToOne(fetch=FetchType.LAZY)
	private Gender gender;

	//bi-directional many-to-one association to PersonProfessional
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="person_professional_id")
	private PersonProfessional personProfessional;

	//bi-directional many-to-one association to Role
	@ManyToOne(fetch=FetchType.LAZY)
	private Role role;

	//bi-directional many-to-one association to Status
	@ManyToOne(fetch=FetchType.LAZY)
	private Status status;

	public User() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstname() {
		return this.firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return this.lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public List<Reservation> getReservations1() {
		return this.reservations1;
	}

	public void setReservations1(List<Reservation> reservations1) {
		this.reservations1 = reservations1;
	}

	public Reservation addReservations1(Reservation reservations1) {
		getReservations1().add(reservations1);
		reservations1.setUser1(this);

		return reservations1;
	}

	public Reservation removeReservations1(Reservation reservations1) {
		getReservations1().remove(reservations1);
		reservations1.setUser1(null);

		return reservations1;
	}

	public List<Reservation> getReservations2() {
		return this.reservations2;
	}

	public void setReservations2(List<Reservation> reservations2) {
		this.reservations2 = reservations2;
	}

	public Reservation addReservations2(Reservation reservations2) {
		getReservations2().add(reservations2);
		reservations2.setUser2(this);

		return reservations2;
	}

	public Reservation removeReservations2(Reservation reservations2) {
		getReservations2().remove(reservations2);
		reservations2.setUser2(null);

		return reservations2;
	}

	public Gender getGender() {
		return this.gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public PersonProfessional getPersonProfessional() {
		return this.personProfessional;
	}

	public void setPersonProfessional(PersonProfessional personProfessional) {
		this.personProfessional = personProfessional;
	}

	public Role getRole() {
		return this.role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public Status getStatus() {
		return this.status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

}