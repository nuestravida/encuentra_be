package com.tesis.encuentra.model.command;

import javax.validation.constraints.Min;
import java.math.BigDecimal;

public class QualificationRequest {
    @Min(value = 0)
    private BigDecimal score;
    private String comment;
    @Min(value = 0)
    private int reservationId;

    public BigDecimal getScore() {
        return score;
    }

    public void setScore(BigDecimal score) {
        this.score = score;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public int getReservationId() {
        return reservationId;
    }

    public void setReservationId(int reservationId) {
        this.reservationId = reservationId;
    }
}
