package com.tesis.encuentra.model.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the files database table.
 * 
 */
@Entity
@Table(name="files")
@NamedQuery(name="File.findAll", query="SELECT f FROM File f")
public class File implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;

	@Column(name="content_type")
	private String contentType;

	private String path;

	//bi-directional many-to-one association to PersonProfessional
	@OneToMany(mappedBy="file")
	private List<PersonProfessional> personProfessionals;

	//bi-directional many-to-one association to Profession
	@OneToMany(mappedBy="file")
	private List<Profession> professions;

	public File() {
	}

	public File(String path) {
		this.path = path;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getContentType() {
		return this.contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getPath() {
		return this.path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public List<PersonProfessional> getPersonProfessionals() {
		return this.personProfessionals;
	}

	public void setPersonProfessionals(List<PersonProfessional> personProfessionals) {
		this.personProfessionals = personProfessionals;
	}

	public PersonProfessional addPersonProfessional(PersonProfessional personProfessional) {
		getPersonProfessionals().add(personProfessional);
		personProfessional.setFile(this);

		return personProfessional;
	}

	public PersonProfessional removePersonProfessional(PersonProfessional personProfessional) {
		getPersonProfessionals().remove(personProfessional);
		personProfessional.setFile(null);

		return personProfessional;
	}

	public List<Profession> getProfessions() {
		return this.professions;
	}

	public void setProfessions(List<Profession> professions) {
		this.professions = professions;
	}

	public Profession addProfession(Profession profession) {
		getProfessions().add(profession);
		profession.setFile(this);

		return profession;
	}

	public Profession removeProfession(Profession profession) {
		getProfessions().remove(profession);
		profession.setFile(null);

		return profession;
	}

}