package com.tesis.encuentra.model.dto;

import com.tesis.encuentra.model.entities.State;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CityDto {
    private int id;
    private String name;
    private State state;
}
