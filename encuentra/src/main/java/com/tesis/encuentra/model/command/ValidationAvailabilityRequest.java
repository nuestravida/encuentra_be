package com.tesis.encuentra.model.command;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.LocalTime;
import java.util.Date;

public class ValidationAvailabilityRequest {
    @Min(value = 1)
    private int categoryId;
    @NotNull(message = "{validation.date.notnull}")
    private Date dateReservation;
    @JsonFormat(pattern = "HH:mm")
    private LocalTime timeReservation;
    @Min(value = 1)
    private int cityId;
    @Min(value = 1)
    private int quantityTimes;

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public Date getDateReservation() {
        return dateReservation;
    }

    public void setDateReservation(Date dateReservation) {
        this.dateReservation = dateReservation;
    }

    public LocalTime getTimeReservation() {
        return timeReservation;
    }

    public void setTimeReservation(LocalTime timeReservation) {
        this.timeReservation = timeReservation;
    }

    public int getCityId() {
        return cityId;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }

    public int getQuantityTimes() {
        return quantityTimes;
    }

    public void setQuantityTimes(int quantityTimes) {
        this.quantityTimes = quantityTimes;
    }
}
