package com.tesis.encuentra.model.dto;

import lombok.Getter;
import lombok.Setter;

import java.sql.Time;
import java.util.Date;

@Setter
@Getter
public class ReservationDto {
    private int id;
    private Date date;
    private Time time;
    private String address;
    private String phone;
    private UserDto user2;
    private String observations;
    private FixedDataDto category;
}
