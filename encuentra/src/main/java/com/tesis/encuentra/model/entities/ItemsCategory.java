package com.tesis.encuentra.model.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the items_categories database table.
 * 
 */
@Entity
@Table(name="items_categories")
@NamedQuery(name="ItemsCategory.findAll", query="SELECT i FROM ItemsCategory i")
public class ItemsCategory implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;

	//bi-directional many-to-one association to Category
	@ManyToOne(fetch=FetchType.LAZY)
	private Category category;

	//bi-directional many-to-one association to Item
	@ManyToOne(fetch=FetchType.LAZY)
	private Item item;

	public ItemsCategory() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Category getCategory() {
		return this.category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public Item getItem() {
		return this.item;
	}

	public void setItem(Item item) {
		this.item = item;
	}

}