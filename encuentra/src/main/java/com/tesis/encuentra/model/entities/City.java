package com.tesis.encuentra.model.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the cities database table.
 * 
 */
@Entity
@Table(name="cities")
@NamedQuery(name="City.findAll", query="SELECT c FROM City c")
public class City implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;

	private String name;

	//bi-directional many-to-one association to State
	@ManyToOne(fetch=FetchType.LAZY)
	private State state;

	//bi-directional many-to-one association to PersonProfessionalsCity
	@OneToMany(mappedBy="city")
	private List<PersonProfessionalsCity> personProfessionalsCities;

	public City() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public State getState() {
		return this.state;
	}

	public void setState(State state) {
		this.state = state;
	}

	public List<PersonProfessionalsCity> getPersonProfessionalsCities() {
		return this.personProfessionalsCities;
	}

	public void setPersonProfessionalsCities(List<PersonProfessionalsCity> personProfessionalsCities) {
		this.personProfessionalsCities = personProfessionalsCities;
	}

	public PersonProfessionalsCity addPersonProfessionalsCity(PersonProfessionalsCity personProfessionalsCity) {
		getPersonProfessionalsCities().add(personProfessionalsCity);
		personProfessionalsCity.setCity(this);

		return personProfessionalsCity;
	}

	public PersonProfessionalsCity removePersonProfessionalsCity(PersonProfessionalsCity personProfessionalsCity) {
		getPersonProfessionalsCities().remove(personProfessionalsCity);
		personProfessionalsCity.setCity(null);

		return personProfessionalsCity;
	}

}