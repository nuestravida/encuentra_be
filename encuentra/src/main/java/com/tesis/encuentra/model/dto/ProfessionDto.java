package com.tesis.encuentra.model.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProfessionDto {
    private int id;
    private String name;
    private String description;
    private FileDto file;
}
