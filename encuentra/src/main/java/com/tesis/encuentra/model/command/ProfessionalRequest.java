package com.tesis.encuentra.model.command;

import com.tesis.encuentra.model.entities.Gender;
import com.tesis.encuentra.model.entities.IdentificationType;

import javax.validation.constraints.NotBlank;

public class ProfessionalRequest {
    @NotBlank(message = "{validation.email.notEmpty}")
    private String email;
    @NotBlank(message = "{validation.firstName.notEmpty}")
    private String firstName;
    @NotBlank(message = "{validation.lastName.notEmpty}")
    private String lastName;
    @NotBlank(message = "{validation.password.notEmpty}")
    private String password;
    private String phone;
    @NotBlank(message = "{validation.gender.not-blank}")
    private Gender gender;
    @NotBlank(message = "{validation.identificationType.notBlank}")
    private IdentificationType identificationType;
    @NotBlank(message = "{validation.identificationNumber.notBlank}")
    private String identificationNumber;

    public String getEmail() {
        return email;
    }

    public IdentificationType getIdentificationType() {
        return identificationType;
    }

    public void setIdentificationType(IdentificationType identificationType) {
        this.identificationType = identificationType;
    }

    public String getIdentificationNumber() {
        return identificationNumber;
    }

    public void setIdentificationNumber(String identificationNumber) {
        this.identificationNumber = identificationNumber;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

}
