package com.tesis.encuentra.model.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class FileDto {
    private int id;
    private String path;

}
