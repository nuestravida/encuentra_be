package com.tesis.encuentra.model.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the identification_types database table.
 * 
 */
@Entity
@Table(name="identification_types")
@NamedQuery(name="IdentificationType.findAll", query="SELECT i FROM IdentificationType i")
public class IdentificationType implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;

	private String name;

	//bi-directional many-to-one association to PersonProfessional
	@OneToMany(mappedBy="identificationType")
	private List<PersonProfessional> personProfessionals;

	public IdentificationType() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<PersonProfessional> getPersonProfessionals() {
		return this.personProfessionals;
	}

	public void setPersonProfessionals(List<PersonProfessional> personProfessionals) {
		this.personProfessionals = personProfessionals;
	}

	public PersonProfessional addPersonProfessional(PersonProfessional personProfessional) {
		getPersonProfessionals().add(personProfessional);
		personProfessional.setIdentificationType(this);

		return personProfessional;
	}

	public PersonProfessional removePersonProfessional(PersonProfessional personProfessional) {
		getPersonProfessionals().remove(personProfessional);
		personProfessional.setIdentificationType(null);

		return personProfessional;
	}

}