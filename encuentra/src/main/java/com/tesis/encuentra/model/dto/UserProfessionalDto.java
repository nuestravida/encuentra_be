package com.tesis.encuentra.model.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserProfessionalDto {
    public String id;
    public String firstname;
    public String lastname;
}
