package com.tesis.encuentra.repository;

import com.tesis.encuentra.model.entities.SocialNetwork;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SocialNetworkRepository extends JpaRepository<SocialNetwork, Integer> {

    List<SocialNetwork> findByPersonProfessionalId(int id);

}
