package com.tesis.encuentra.repository;

import com.tesis.encuentra.model.entities.Qualification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface QualificationRepository extends JpaRepository<Qualification, Long> {

    @Query("SELECT Q FROM Qualification Q JOIN FETCH Q.reservation R JOIN FETCH R.user1 U WHERE U.id = ?1")
    List<Qualification> findByUserId(int idUser);

}
