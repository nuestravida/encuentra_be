package com.tesis.encuentra.repository;

import com.tesis.encuentra.model.entities.User;
import com.tesis.encuentra.util.Constant;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class PersonProfessionalRepositoryImpl implements PersonProfessionalRepositoryCustom {

    @PersistenceContext
    EntityManager entityManager;

    @Override
    public List<User> validateAvailabilityProfessional(int categoryId, Date dateService, Time timeStartService, Time timeEndService, int statusId) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

        Query query = entityManager.createNativeQuery("select u.*q from tesis_karenbd.person_professionals pp " +
                "inner join tesis_karenbd.users u on (u.person_professional_id = pp.id)" +
                "inner join tesis_karenbd.schedules s on (s.person_professional_id = pp.id) " +
                "inner join tesis_karenbd.person_professionals_cities ppc on (ppc.person_professional_id = pp.id) " +
                "inner join tesis_karenbd.categories_person_professionals cpp on (cpp.person_professional_id = pp.id) " +
                "where u.status_id = ? and s.date = ? and (s.all_day = 0 OR s.time_start <= ? and s.time_end >= ?) " +
                "and cpp.category_id = ?", User.class);
        query.setParameter(1, Constant.STATUS_USER_ACTIVE);
        query.setParameter(2, format.format(dateService));
        query.setParameter(3, timeStartService.toString());
        query.setParameter(4, timeEndService.toString());
        query.setParameter(5, categoryId);
        return query.getResultList();
    }
}
