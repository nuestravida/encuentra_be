package com.tesis.encuentra.repository;

import com.tesis.encuentra.model.entities.User;

import java.sql.Time;
import java.util.Date;
import java.util.List;

public interface PersonProfessionalRepositoryCustom {

    List<User> validateAvailabilityProfessional(int categoryId, Date dateService, Time timeStartService, Time timeEndService, int statusId);

}
