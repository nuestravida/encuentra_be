package com.tesis.encuentra.repository;

import com.tesis.encuentra.model.entities.Item;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface SubItemRepository extends JpaRepository<Item, Integer> {
    @Query("SELECT DISTINCT I FROM Item AS I INNER JOIN Subitem AS S ON(S.item.id = I.id) INNER JOIN ItemsCategory AS IC ON(IC.item.id = I.id) WHERE IC.category.id = ?1")
    List<Item> findBySubItemByCategoryId(int idCategory);
}
