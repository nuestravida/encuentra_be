package com.tesis.encuentra.repository;

import com.tesis.encuentra.model.entities.ItemOption;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ItemOptionRepository extends JpaRepository<ItemOption, Integer> {

    @Query("SELECT DISTINCT I FROM ItemOption I INNER JOIN I.options O WHERE O.profession.id = ?1")
    List<ItemOption> findByProfession(int idProfession);

}
