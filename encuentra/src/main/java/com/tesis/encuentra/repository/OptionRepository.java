package com.tesis.encuentra.repository;

import com.tesis.encuentra.model.entities.Option;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OptionRepository extends JpaRepository<Option, Integer> {

    List<Option> findByProfessionId(int id);

}
