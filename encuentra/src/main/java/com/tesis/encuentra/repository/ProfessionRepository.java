package com.tesis.encuentra.repository;

import com.tesis.encuentra.model.entities.Profession;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProfessionRepository extends JpaRepository<Profession, Integer> {

    @Query("SELECT P FROM Profession P JOIN FETCH P.file")
    List<Profession> findFileAll();

}
