package com.tesis.encuentra.repository;

import com.tesis.encuentra.model.entities.Reservation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReservationRepository extends JpaRepository<Reservation, Integer> {

    @Query("SELECT R FROM Reservation R JOIN FETCH R.user2 WHERE R.user1.id = ?1 AND R.status.id = ?2")
    List<Reservation> findByUser(int idUser, int idStatus);

}
