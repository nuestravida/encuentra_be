package com.tesis.encuentra.repository;

import com.tesis.encuentra.model.entities.PersonProfessional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonProfessionalRepository extends JpaRepository<PersonProfessional, Long>, PersonProfessionalRepositoryCustom {

//    @Query("SELECT U FROM PersonProfessional PP " +
//            "INNER JOIN User U ON(U.personProfessional = PP.id) " +
//            "INNER JOIN Schedule S ON(S.personProfessional = PP.id) " +
//            "INNER JOIN PersonProfessionalsCity PPC ON(PP.id = PPC.personProfessional) " +
//            "INNER JOIN CategoriesPersonProfessional CPP ON(PP.id = CPP.personProfessional) "+
//            "WHERE S.date = ?2 AND (S.allDay = 0 OR S.timeStart <= ?3 AND S.timeEnd >= ?4) " +
//            "AND U.status.id = ?5 " +
//            "AND CPP.category.id = ?1")
//    @Query(value = "select u.id,u.firstname,u.  lastname from tesis_karenbd.person_professionals pp " +
//            "inner join tesis_karenbd.users u on (u.person_professional_id = pp.id)" +
//            "inner join tesis_karenbd.schedules s on (s.person_professional_id = pp.id) " +
//            "inner join tesis_karenbd.person_professionals_cities ppc on (ppc.person_professional_id = pp.id) " +
//            "inner join tesis_karenbd.categories_person_professionals cpp on (cpp.person_professional_id = pp.id) " +
//            "where u.status_id = ?5 and s.date = ?2 and (s.all_day = 0 OR s.time_start <= ?3 and s.time_end >= ?4) " +
//            "and cpp.category_id = ?1", nativeQuery = true)
//
}
