package com.tesis.encuentra.util;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;


public class UtilSecurity {

    public static String generatePasswordRecover(String password) {
        BCryptPasswordEncoder brBCryptPasswordEncoder = new BCryptPasswordEncoder();
        return brBCryptPasswordEncoder.encode(password);
    }
}
