package com.tesis.encuentra.util;

public class Constant {
    public static int STATUS_USER_ACTIVE = 1;
    public static int STATUS_USER_REGISTERED = 1;
    public static int ROLE_USER = 2;
    public static int ROLE_PROFESSIONAL = 3;
    public static int GENDER = 1;

    //Status reservations
    public static int STATUS_RESERVATION_FINISHED = 4;
    public static int STATUS_RESERVATION_RESERVED = 3;
}
