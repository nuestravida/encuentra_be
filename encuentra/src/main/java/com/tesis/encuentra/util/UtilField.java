package com.tesis.encuentra.util;

import java.util.Date;

public class UtilField {
    public static boolean isNullOrEmpty(String string) {
        return string == null || string.isEmpty();
    }

    public static Date expirationUrlAws() {
        java.util.Date expiration = new java.util.Date();
        long expTimeMillis = expiration.getTime();
        expTimeMillis += 1000 * 60 * 60;
        expiration.setTime(expTimeMillis);
        return expiration;
    }
}
