package com.tesis.encuentra.controller;

import com.tesis.encuentra.config.JwtToken;
import com.tesis.encuentra.exception.BadRequestException;
import com.tesis.encuentra.model.command.LoginRequest;
import com.tesis.encuentra.model.dto.UserDto;
import com.tesis.encuentra.service.JwtUserDetailsService;
import com.tesis.encuentra.service.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("/auth")
public class AuthController {
    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    private JwtUserDetailsService jwtUserDetailsService;

    @Autowired
    private UserServiceImpl userService;

    @Autowired
    private JwtToken jwtToken;

    @PostMapping()
    public ResponseEntity<?> Post(@Valid @RequestBody LoginRequest loginRequest) throws Exception {
        authenticate(loginRequest.getEmail(), loginRequest.getPassword());

        final UserDetails userDetails = jwtUserDetailsService
                .loadUserByUsername(loginRequest.getEmail());
        final String token = jwtToken.generateToken(userDetails);

        Optional<UserDto> userDto = userService.findByUsername(loginRequest.getEmail());
        userDto.orElseThrow(() -> new BadRequestException("Se presentó un error al obtener la información del usuario"));
        userDto.get().token = token;



        return ResponseEntity.ok(userDto.get());
    }

    /**
     * @param username
     * @param password
     * @throws Exception
     */
    private void authenticate(String username, String password) throws Exception {

        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new Exception("Usuario deshabilitado", e);
        } catch (BadCredentialsException e) {
            throw new Exception("Usuario o contraseña incorrectos", e);
        } catch (AuthenticationException e) {
            throw new Exception("Error de autenticación", e);
        }
    }
}
