package com.tesis.encuentra.controller;

import com.tesis.encuentra.service.ProfessionServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/professions")
public class ProfessionController {

    @Autowired
    ProfessionServiceImpl professionService;

    @GetMapping()
    public ResponseEntity<?> Get() {
        return ResponseEntity.ok(professionService.getAll());
    }
}
