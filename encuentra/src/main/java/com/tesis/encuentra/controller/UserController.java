package com.tesis.encuentra.controller;


import com.tesis.encuentra.exception.BadRequestException;
import com.tesis.encuentra.model.command.UserRequest;
import com.tesis.encuentra.model.command.RecoverPasswordRequest;
import com.tesis.encuentra.model.entities.User;
import com.tesis.encuentra.repository.UserRepository;
import com.tesis.encuentra.service.RecoverPasswordServiceImpl;
import com.tesis.encuentra.service.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Locale;


@RestController
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserServiceImpl userService;

    @Autowired
    MessageSource messageSource;

    @Autowired
    private RecoverPasswordServiceImpl recoverPasswordService;

    @GetMapping("/users/{username}")
    public User Get(@PathVariable("username") String username) {
        return userRepository.findByEmail(username).get();
    }

    @PostMapping("/users")
    public ResponseEntity<?> Post(@Valid @RequestBody UserRequest userRequest) {
        return ResponseEntity.ok(userService.save(userRequest));
    }

    @PutMapping("/users/{id}")
    public ResponseEntity<?> Put(@Valid @RequestBody UserRequest userRequest, @PathVariable(value = "id") int id) {

        userRepository.findById(id).orElseThrow(() -> new BadRequestException("El id no se encuentra registrado"));

        if (userRepository.findByEmail(userRequest.getEmail()).isPresent()) {
            throw new BadRequestException(messageSource.getMessage("user.validation.email_no_exist", null, Locale.ROOT));
        }
        return ResponseEntity.ok(userService.save(userRequest));
    }

    @PostMapping("/recover-password")
    public ResponseEntity<?> Post(@Valid @RequestBody RecoverPasswordRequest recoverPasswordDto) {
        recoverPasswordService.recoverPassword(recoverPasswordDto);
        return ResponseEntity.ok("");
    }
}
