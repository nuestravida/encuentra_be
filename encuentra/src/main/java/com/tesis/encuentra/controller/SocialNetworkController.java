package com.tesis.encuentra.controller;

import com.tesis.encuentra.model.dto.SocialNetworkDto;
import com.tesis.encuentra.service.SocialNetworkServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class SocialNetworkController {

    @Autowired
    private SocialNetworkServiceImpl socialNetworkServiceImpl;

    @GetMapping("/person_professionals/{id-person-professional}/social-networks")
    public List<SocialNetworkDto> Get(@PathVariable("id-person-professional") int idPersonProfessional) {
        return socialNetworkServiceImpl.getSocialNetworkByPersonProfessional(idPersonProfessional);
    }

}
