package com.tesis.encuentra.controller;

import com.mailjet.client.errors.MailjetException;
import com.mailjet.client.errors.MailjetSocketTimeoutException;
import com.tesis.encuentra.service.CityServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
public class CityController {
    @Autowired
    CityServiceImpl cityService;

    @GetMapping("/states/{id-state}/cities")
    public ResponseEntity<?> Get(@PathVariable("id-state") int idState) {
        return ResponseEntity.ok(cityService.getByIdState(idState));
    }

    @GetMapping("/cities/email")
    public ResponseEntity<?> Get() throws MailjetSocketTimeoutException, MailjetException, IOException {
        return ResponseEntity.ok("Se envió correctamente el correo");
    }
}
