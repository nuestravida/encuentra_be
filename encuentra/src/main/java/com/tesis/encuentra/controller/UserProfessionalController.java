package com.tesis.encuentra.controller;

import com.tesis.encuentra.model.command.UserProfessionalRequest;
import com.tesis.encuentra.service.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/user-professionals")
public class UserProfessionalController {

    @Autowired
    private UserServiceImpl userService;

    @PostMapping
    public ResponseEntity<?> Post(@Valid @RequestBody UserProfessionalRequest userProfessionalRequest) {
        return ResponseEntity.ok(userService.saveUserProfessional(userProfessionalRequest));
    }
}
