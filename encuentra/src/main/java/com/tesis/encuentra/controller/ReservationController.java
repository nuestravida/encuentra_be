package com.tesis.encuentra.controller;

import com.tesis.encuentra.service.ReservationServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class ReservationController {

    @Autowired
    private ReservationServiceImpl reservationService;

    @GetMapping("/users/{id-user}/reservations")
    public ResponseEntity<?> Get(@PathVariable("id-user") int idUser) {
        return ResponseEntity.ok(reservationService.getByIdUser(idUser));
    }

    @PatchMapping("/reservations/{id}")
    public ResponseEntity<?> Patch(@PathVariable("id") int id) {
        return ResponseEntity.ok(reservationService.updateState(id));
    }
}
