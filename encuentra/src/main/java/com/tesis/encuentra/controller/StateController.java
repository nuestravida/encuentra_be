package com.tesis.encuentra.controller;

import com.tesis.encuentra.service.StateServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StateController {
    @Autowired
    StateServiceImpl stateService;

    @GetMapping("/countries/{id-country}/states")
    public ResponseEntity<?> Get(@PathVariable("id-country") int idCountry) {
        return ResponseEntity.ok(stateService.getByIdCountry(idCountry));
    }
}
