package com.tesis.encuentra.controller;

import com.tesis.encuentra.model.command.ValidationAvailabilityRequest;
import com.tesis.encuentra.model.entities.User;
import com.tesis.encuentra.repository.PersonProfessionalRepository;
import com.tesis.encuentra.service.ValidateAvailabilityServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;


@RestController
@RequestMapping("/validate-availability")
public class ValidateAvailabilityController {

    @Autowired
    ValidateAvailabilityServiceImpl validateAvailabilityServiceImpl;

    @PostMapping
    public ResponseEntity<?> Post(@Valid @RequestBody ValidationAvailabilityRequest validationAvailabilityRequest) {
        return ResponseEntity.ok(validateAvailabilityServiceImpl.validateAvailabilityProfessional(validationAvailabilityRequest));
    }
}
