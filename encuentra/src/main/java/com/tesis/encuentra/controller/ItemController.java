package com.tesis.encuentra.controller;

import com.tesis.encuentra.service.ItemServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/items")
public class ItemController {

    @Autowired
    private ItemServiceImpl itemService;

    @GetMapping("/{id-category}")
    public ResponseEntity<?> Get(@PathVariable("id-category") int idCategory) {
        return ResponseEntity.ok(itemService.getSubItem(idCategory));
    }
}
