package com.tesis.encuentra.controller;

import com.tesis.encuentra.service.CategoryServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CategoryController {

    @Autowired
    CategoryServiceImpl categoryServiceImpl;

    @GetMapping("/professions/{id-profession}/categories")
    public ResponseEntity<?> Get(@PathVariable("id-profession") int idProfession) {
        return ResponseEntity.ok(categoryServiceImpl.findByProfessionId(idProfession));
    }
}
