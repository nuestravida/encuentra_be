package com.tesis.encuentra.controller;

import com.tesis.encuentra.model.dto.ItemOptionDto;
import com.tesis.encuentra.service.ItemOptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/item-options")
public class ItemOptionController {

    @Autowired
    private ItemOptionService itemOptionService;

    @GetMapping("/{id-profession}")
    public List<ItemOptionDto> Get(@PathVariable("id-profession") int idProfession) {
        return itemOptionService.findByProfession(idProfession);
    }
}
