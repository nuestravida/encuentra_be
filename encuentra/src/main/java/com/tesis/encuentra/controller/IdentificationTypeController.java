package com.tesis.encuentra.controller;

import com.tesis.encuentra.service.IdentificationTypeServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/identification-types")
public class IdentificationTypeController {
    @Autowired
    IdentificationTypeServiceImpl identificationTypeService;

    @GetMapping()
    public ResponseEntity<?> Get() {
        return ResponseEntity.ok(identificationTypeService.getAll());
    }
}
