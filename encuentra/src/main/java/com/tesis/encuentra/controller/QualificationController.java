package com.tesis.encuentra.controller;

import com.tesis.encuentra.model.command.QualificationRequest;
import com.tesis.encuentra.service.QualificationServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class QualificationController {

    @Autowired
    private QualificationServiceImpl qualificationService;

    @PostMapping("/qualifications")
    public ResponseEntity<?> Post(@Valid @RequestBody QualificationRequest qualificationRequest) {
        qualificationService.save(qualificationRequest);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping("/users/{id-user}/qualifications")
    public ResponseEntity<?> Get(@PathVariable("id-user") int idUser) {
        return ResponseEntity.ok(qualificationService.getQualificationByReservation(idUser));
    }
}
