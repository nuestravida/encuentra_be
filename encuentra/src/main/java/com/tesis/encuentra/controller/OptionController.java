package com.tesis.encuentra.controller;

import com.tesis.encuentra.model.dto.OptionDto;
import com.tesis.encuentra.service.OptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/options")
public class OptionController {

    @Autowired
    private OptionService optionService;

    @GetMapping("/{id-profession}")
    public List<OptionDto> Get(@PathVariable("id-profession") int idProfession) {
        return optionService.findByProfessionId(idProfession);
    }

}
