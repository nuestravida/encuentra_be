package com.tesis.encuentra;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class EncuentraApplication {

	public static void main(String[] args) {
		SpringApplication.run(EncuentraApplication.class, args);
	}

}
